package test.filechooser;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

import ru.bossnote.filechooser.models.ReturnableFileInfo;
import ru.bossnote.filechooser.ui.activities.FileChooser;

/**
 * Created by Alexandr on 04.03.2015.
 */
public class TestActivity extends Activity {
    public static final String TAG = TestActivity.class.getSimpleName();
    public static final int REQUEST_CODE = 222;
    public static final String RESULT_KEY = "array_list_uri";
    public static final String FULL_VERSION_KEY = "full_version";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            Log.d(TAG, "REQUEST_CODE");

            if (resultCode == RESULT_OK) {
                Log.d(TAG, "RESULT_OK");

                Bundle args = data.getExtras();
                ArrayList<ReturnableFileInfo> array = args.getParcelableArrayList(RESULT_KEY);

                Toast.makeText(this, "Selected " + array.size() + " file(-s)", Toast.LENGTH_SHORT).show();

                for (ReturnableFileInfo value : array)
                    Log.d(TAG, "name = " + value.getName());

            } else if (resultCode == RESULT_CANCELED)
                Log.d(TAG, "RESULT_CANCELED");
        }
    }

    public void onChoose(View view) {
        boolean ifFullVersion = false;
        Intent intent = new Intent(this, FileChooser.class);
        intent.putExtra(FULL_VERSION_KEY, ifFullVersion);
        startActivityForResult(intent, REQUEST_CODE);
    }

    public void onChooseFull(View view) {
        boolean ifFullVersion = true;
        Intent intent = new Intent(this, FileChooser.class);
        intent.putExtra(FULL_VERSION_KEY, ifFullVersion);
        startActivityForResult(intent, REQUEST_CODE);
    }
}
