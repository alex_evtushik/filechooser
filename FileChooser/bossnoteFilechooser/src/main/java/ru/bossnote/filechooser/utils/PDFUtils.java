package ru.bossnote.filechooser.utils;


import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfRenderer;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.widget.ImageView;

import com.qoppa.android.pdfProcess.PDFDocument;
import com.qoppa.android.pdfViewer.fonts.StandardFontTF;

import java.io.File;
import java.io.IOException;

/**
 * Created by Alexandr on 13.03.2015.
 */
public class PDFUtils {
    public static final String TAG = PDFUtils.class.getSimpleName();

    private PDFUtils() {
    }

    public static void getThumbnailQoppa(Context context, ImageView imageView) {
        String pathOne = "/storage/emulated/0/progit.ru.pdf";
        try {
            //this static allows the sdk to access font assets,
            //it must be set prior to utilizing libraries
            StandardFontTF.mAssetMgr = context.getAssets();

            // Load a document and get the first page
            PDFDocument pdf = new PDFDocument(pathOne, null);
            com.qoppa.android.pdfProcess.PDFPage page = pdf.getPage(0);

            // Create a bitmap and canvas to draw the page into
            int width = (int) Math.ceil(page.getDisplayWidth());
            int height = (int) Math.ceil(page.getDisplayHeight());
            Bitmap bm = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

            // Create canvas to draw into the bitmap
            Canvas c = new Canvas(bm);

            // Fill the bitmap with a white background
            Paint whiteBgnd = new Paint();
            whiteBgnd.setColor(Color.WHITE);
            whiteBgnd.setStyle(Paint.Style.FILL);
            c.drawRect(0, 0, width, height, whiteBgnd);

            // paint the page into the canvas
            page.paintPage(c);

            imageView.setImageBitmap(bm);
            // Save the bitmap
//            OutputStream outStream = new FileOutputStream("/sdcard/output.jpg");
//            bm.compress(CompressFormat.JPEG, 80, outStream);
//            outStream.close();

        } catch (Exception e) {
            Log.e("error", Log.getStackTraceString(e));
        }
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void getThumbnailOnLollipop(Context context, ImageView imageView) {

        try {
            String pathOne = "/storage/emulated/0/Download/sample.pdf";
//
            File file = new File(pathOne);

            if (file != null)
                Log.d(TAG, "name pdf = " + file.getName());

            ParcelFileDescriptor descriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
//            ParcelFileDescriptor descriptor = context.getAssets().openFd("sample.pdf").getParcelFileDescriptor();


            PdfRenderer renderer = new PdfRenderer(descriptor);

            PdfRenderer.Page page = renderer.openPage(0);

            Bitmap bitmap = Bitmap.createBitmap(page.getWidth(), page.getHeight(), Bitmap.Config.ARGB_8888);

            page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);

            if (bitmap != null)
                Log.d(TAG, "I have bitmap!");

            imageView.setImageBitmap(bitmap);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
