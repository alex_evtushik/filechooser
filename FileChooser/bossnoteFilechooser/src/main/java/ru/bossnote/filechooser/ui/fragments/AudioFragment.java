package ru.bossnote.filechooser.ui.fragments;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import ru.bossnote.filechooser.R;
import ru.bossnote.filechooser.adapters.AudioDataAdapter;
import ru.bossnote.filechooser.adapters.SimpleSpinnerAdapter;
import ru.bossnote.filechooser.constants.Constants;
import ru.bossnote.filechooser.cursors.AudioLoaderCallback;
import ru.bossnote.filechooser.listeners.OnAlbumClickListener;
import ru.bossnote.filechooser.listeners.OnFinishCallback;
import ru.bossnote.filechooser.models.GalleryInfo;
import ru.bossnote.filechooser.models.ReturnableFileInfo;

/**
 * Created by Alexandr on 27.02.2015.
 */
public class AudioFragment extends BaseFragment {
    public static final String TAG = AudioFragment.class.getSimpleName();
    private RecyclerView mRecyclerView;
    private LayoutManager mLayoutManager;
    private AudioDataAdapter mAdapter;
    private AudioLoaderCallback mCursorLoader;
    private LoaderManager mLoaderManager;
    private Toolbar mToolbar;
    private View mSpinnerContainer;
    private Spinner mSpinner;
    private SimpleSpinnerAdapter mSpinnerAdapter;
    private ArrayList<GalleryInfo> mAlbumDepth;

    public AudioFragment() {
    }

    public static AudioFragment newInstance() {
        AudioFragment fragment = new AudioFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.recycler_view_fragment, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new AudioDataAdapter(null);
        mLoaderManager = getLoaderManager();
        mCursorLoader = new AudioLoaderCallback(getActivity(), mAdapter);
        mAdapter.setOnAlbumClickListener(onAlbumClick);
        mAdapter.setOnFinishCallback(callback);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mSpinnerContainer = LayoutInflater.from(getActivity()).inflate(R.layout.toolbar_spinner, mToolbar, false);
        mToolbar.addView(mSpinnerContainer);
        mSpinner = (Spinner) mSpinnerContainer.findViewById(R.id.toolbar_spinner);
        mSpinnerAdapter = new SimpleSpinnerAdapter(getActivity());
        mSpinner.setAdapter(mSpinnerAdapter);
        mSpinner.setOnItemSelectedListener(onItemSelected);

        if (savedInstanceState != null)
            mAlbumDepth = savedInstanceState.getParcelableArrayList(TAG);
        else {
            mAlbumDepth = new ArrayList<>();
            mAlbumDepth.add(new GalleryInfo(getActivity().getResources().getString(R.string.music), "", true));
        }

        updateCurrentAlbum();

        return rootView;
    }

    private void updateCurrentAlbum() {
        mSpinnerAdapter.swapData(mAlbumDepth);
        mSpinner.setSelection(mAlbumDepth.size() - 1);
        boolean isRoot = mAlbumDepth.get(mAlbumDepth.size() - 1).isRoot();

        if (isRoot) {
            mLoaderManager.destroyLoader(AudioLoaderCallback.AUDIO_LOADER);
            mLoaderManager.initLoader(AudioLoaderCallback.ALBUM_LOADER, null, mCursorLoader);
        } else {
            mLoaderManager.destroyLoader(AudioLoaderCallback.ALBUM_LOADER);
            String albumId = mAlbumDepth.get(mAlbumDepth.size() - 1).getAlbumId();
            mCursorLoader.setPictureWhere(albumId);
            mLoaderManager.initLoader(AudioLoaderCallback.AUDIO_LOADER, null, mCursorLoader);
        }

        if (mAlbumDepth.size() <= 1) {
            mSpinner.setClickable(false);
        } else {
            mSpinner.setClickable(true);
        }

    }

    @Override
    public boolean onBackPressed() {
        if (mAdapter.getActionMode() != null) {
            mAdapter.getActionMode().finish();
            return true;
        } else if (mAlbumDepth.size() > 1) {
            mAlbumDepth.remove(mAlbumDepth.size() - 1);
            updateCurrentAlbum();
            return true;
        } else {
            getActivity().setResult(Activity.RESULT_CANCELED);
            getActivity().finish();
            return false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mToolbar.removeView(mSpinnerContainer);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(TAG, new ArrayList<>(mAlbumDepth));
    }

    private AdapterView.OnItemSelectedListener onItemSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            for (int i = mAlbumDepth.size() - 1; i > position; i--) {
                mAlbumDepth.remove(i);
            }
            updateCurrentAlbum();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private OnAlbumClickListener onAlbumClick = new OnAlbumClickListener() {
        @Override
        public void onAlbumClick(String albumName, String albumId) {
            mAlbumDepth.add(new GalleryInfo(albumName, albumId, false));
            updateCurrentAlbum();
        }
    };

    private OnFinishCallback callback = new OnFinishCallback() {
        @Override
        public void onFinish(List<ReturnableFileInfo> array) {
            if (array.size() == 0) {
                getActivity().setResult(Activity.RESULT_CANCELED);
                getActivity().finish();
            } else {
                Bundle args = new Bundle();
                args.putParcelableArrayList(Constants.RESULT_KEY, new ArrayList<>(array));
                Intent data = new Intent();
                data.putExtras(args);
                getActivity().setResult(Activity.RESULT_OK, data);
                getActivity().finish();
            }
        }
    };
}
