package ru.bossnote.filechooser.listeners;

import java.util.List;

import ru.bossnote.filechooser.models.ReturnableFileInfo;

/**
 * Created by Alexandr on 04.03.2015.
 */
public interface OnFinishCallback {

    public void onFinish(List<ReturnableFileInfo> array);
}
