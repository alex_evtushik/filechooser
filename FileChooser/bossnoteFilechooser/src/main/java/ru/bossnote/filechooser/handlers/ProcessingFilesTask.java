package ru.bossnote.filechooser.handlers;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

import ru.bossnote.filechooser.listeners.OnFinishCallback;
import ru.bossnote.filechooser.models.FileInfo;
import ru.bossnote.filechooser.models.ReturnableFileInfo;
import ru.bossnote.filechooser.utils.FileTypeUtils;

/**
 * Created by Alexandr on 14.03.2015.
 */
public class ProcessingFilesTask extends AsyncTask<Void, Void, List<ReturnableFileInfo>> {
    private final int DELAY_MILLIS = 100;
    private List<FileInfo> mItems;
    private List<Integer> mSelectedPosition;
    private FrameLayout mProgressBar;
    private OnFinishCallback onFinishCallback;
    private static Handler mHandler;
    private static Runnable mTask;

    public ProcessingFilesTask(List<FileInfo> items, List<Integer> selectedPosition, FrameLayout progressBar, OnFinishCallback callback) {
        mItems = items;
        mSelectedPosition = selectedPosition;
        mProgressBar = progressBar;
        onFinishCallback = callback;
        mHandler = new Handler();
    }

    @Override
    protected void onPreExecute() {
        mTask = new Runnable() {
            @Override
            public void run() {
                mProgressBar.setVisibility(View.VISIBLE);
            }
        };
        mHandler.postDelayed(mTask, DELAY_MILLIS);
    }

    @Override
    protected List<ReturnableFileInfo> doInBackground(Void... params) {
        List<ReturnableFileInfo> selectedFilesUri = new ArrayList<>();

        for (int i = mSelectedPosition.size() - 1; i >= 0; i--) {
            calculateSelectedFiles(mItems, mSelectedPosition.get(i), selectedFilesUri);
        }

        return selectedFilesUri;
    }

    @Override
    protected void onPostExecute(List<ReturnableFileInfo> list) {
        mHandler.removeCallbacks(mTask);
        mProgressBar.setVisibility(View.GONE);
        onFinishCallback.onFinish(list);
    }


    private void calculateSelectedFiles(List<FileInfo> items, int index, List<ReturnableFileInfo> outSelectedFiles) {
        if (items.get(index).isDirectiry()) {
            String path = items.get(index).getPath();
            ArrayList<FileInfo> innerItems = ContentHandler.getFiles(path);
            for (int i = 0; i < innerItems.size(); i++) {
                calculateSelectedFiles(innerItems, i, outSelectedFiles);
            }
        } else {
            String uri = items.get(index).getFileUri();
            String name = items.get(index).getName();
            long size = Long.parseLong(items.get(index).getData());
            String mimeType = FileTypeUtils.getFileMimeType(name);

            outSelectedFiles.add(new ReturnableFileInfo(uri, name, size, mimeType));

            Log.d("TAG", "name = " + name + ", size = " + size + ", mimeType = " + mimeType);

        }
    }
}
