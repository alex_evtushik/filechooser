package ru.bossnote.filechooser.constants;

/**
 * Created by Alexandr on 10.03.2015.
 */
public class Constants {
    public static final int PICTURE = 0;
    public static final int VIDEO = 1;
    public static final int AUDIO = 2;
    public static final long DELAY_MILLIS = 400L;

    public static final String RESULT_KEY = "array_list_uri";
    public static final String FULL_VERSION_KEY = "full_version";

    private Constants() {}
}
