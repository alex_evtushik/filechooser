package ru.bossnote.filechooser.listeners;

import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Alexandr on 13.03.2015.
 */
public class PauseOnScrollListener extends RecyclerView.OnScrollListener {

    public static final String TAG = PauseOnScrollListener.class.getSimpleName();

    private ImageLoader mImageLoader;

    public PauseOnScrollListener(ImageLoader imageLoader) {
        mImageLoader = imageLoader;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        switch (newState) {
            case RecyclerView.SCROLL_STATE_IDLE:
                mImageLoader.resume();
                break;
            case RecyclerView.SCROLL_STATE_DRAGGING:
                mImageLoader.resume();
                break;
            case RecyclerView.SCROLL_STATE_SETTLING:
                mImageLoader.pause();
                break;
        }
    }
}
