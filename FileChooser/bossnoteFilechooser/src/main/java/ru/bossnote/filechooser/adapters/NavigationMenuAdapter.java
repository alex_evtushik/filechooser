package ru.bossnote.filechooser.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ru.bossnote.filechooser.R;
import ru.bossnote.filechooser.models.NavigationMenuItem;

/**
 * Created by Alexandr on 26.02.2015.
 */
public class NavigationMenuAdapter extends BaseAdapter {

    private ArrayList<NavigationMenuItem> mItems;
    private LayoutInflater mInflater;

    public NavigationMenuAdapter(Context context, ArrayList<NavigationMenuItem> items) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItems = items;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder = null;

        if (view == null) {
            holder = new ViewHolder();
            view = mInflater.inflate(R.layout.navigation_item, parent, false);
            holder.icon = (ImageView) view.findViewById(R.id.item_icon);
            holder.text = (TextView) view.findViewById(R.id.item_text_audio);
            view.setTag(holder);
        } else
            holder = (ViewHolder) view.getTag();

        holder.icon.setImageResource(mItems.get(position).getImage());
        holder.text.setText(mItems.get(position).getText());

        return view;
    }

    private class ViewHolder {
        ImageView icon;
        TextView text;
    }
}
