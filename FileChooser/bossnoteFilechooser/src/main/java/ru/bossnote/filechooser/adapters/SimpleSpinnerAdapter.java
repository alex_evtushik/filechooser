package ru.bossnote.filechooser.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ru.bossnote.filechooser.R;
import ru.bossnote.filechooser.models.SpinnerItem;

/**
 * Created by Ruslan on 07.03.2015.
 */
public class SimpleSpinnerAdapter extends BaseAdapter {

    public static final String TAG = SimpleSpinnerAdapter.class.getSimpleName();
    private LayoutInflater mInflater;
    private List<? extends SpinnerItem> mData;

    public SimpleSpinnerAdapter(Context context) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void swapData(List<? extends SpinnerItem> data) {
        this.mData = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if(mData == null) return 0;
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.toolbar_spinner_item_title, parent, false);
        }
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
        tv.setText(((SpinnerItem)getItem(position)).getName());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.toolbar_spinner_item_dropdown, parent, false);
        }

        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
        tv.setText(((SpinnerItem)getItem(position)).getName());
        ImageView imageView = (ImageView) convertView.findViewById(R.id.arrow_forward);

        if (position == 0) {
            imageView.setVisibility(View.GONE);
        } else {
            imageView.setVisibility(View.VISIBLE);
        }

        if (position == (mData.size() - 1)) {
            tv.setVisibility(View.GONE);
            imageView.setVisibility(View.GONE);
        }   else
            tv.setVisibility(View.VISIBLE);

        parent.setVerticalScrollBarEnabled(false);
        return convertView;
    }
}