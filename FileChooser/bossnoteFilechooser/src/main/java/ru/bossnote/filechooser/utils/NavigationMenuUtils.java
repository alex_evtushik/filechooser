package ru.bossnote.filechooser.utils;

import android.content.res.Resources;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ru.bossnote.filechooser.R;
import ru.bossnote.filechooser.models.NavigationMenuItem;

/**
 * Created by Alexandr on 10.03.2015.
 */
public class NavigationMenuUtils {

    private NavigationMenuUtils() {
    }

    public static ArrayList<NavigationMenuItem> getListMenu(Resources resources) {
        ArrayList<NavigationMenuItem> items = new ArrayList<>();
        items.add(new NavigationMenuItem(R.drawable.ic_picture_menu_item, resources.getString(R.string.pictures)));
        items.add(new NavigationMenuItem(R.drawable.ic_video_menu_item, resources.getString(R.string.movies)));
        items.add(new NavigationMenuItem(R.drawable.ic_audio_menu_item, resources.getString(R.string.music)));

        getAllStorage(items, resources);

        return items;
    }

    private static void getAllStorage(ArrayList<NavigationMenuItem> items, Resources resources) {
        List<StorageUtils.StorageInfo> storageUtils = StorageUtils.getStorageList();
        File[] files = AllStorageAware.getStorageDirectories();
        ArrayList<String> internalCount = new ArrayList<>();
        ArrayList<String> externalCount = new ArrayList<>();

        if (storageUtils.size() != 0) {
            for (StorageUtils.StorageInfo value : storageUtils) {
                if (value.removable) {
                    externalCount.add(value.path);
                } else {
                    internalCount.add(value.path);
                }
            }
        }
        if (files.length != 0 && internalCount.size() == 0) {
            int equalCount = 0;
            for (File value : files) {
                for (String path : externalCount) {
                    if (value.getPath().equalsIgnoreCase(path))
                        equalCount++;
                }
                if (equalCount == 0)
                    internalCount.add(value.getPath());
            }

        }
        if (internalCount.size() == 0)
            internalCount.add(AllStorageAware.ROOT_PATH);


        ArrayList<String> downloadDir = getDownloadStorage(internalCount, externalCount);
        //Add download dir menu
        if (downloadDir.size() != 0) {
            items.add(new NavigationMenuItem(R.drawable.ic_download_menu_item,
                    resources.getString(R.string.downloads), downloadDir.get(0)));
        }
        //Add internal storage menu
        if (internalCount.size() != 0)
            items.add(new NavigationMenuItem(R.drawable.ic_internal_storage_menu_item,
                    resources.getString(R.string.internal_storage), internalCount.get(0)));
        //Add external storage menu
        if (externalCount.size() != 0) {
            String externalName;

            for (int i = 0; i < externalCount.size(); i++) {
                if (externalCount.size() == 1) {
                    externalName = resources.getString(R.string.external_storage);
                } else {
                    externalName = resources.getString(R.string.external_storage) + " " + (i + 2);
                }
                items.add(new NavigationMenuItem(R.drawable.ic_sd_storage_menu_item, externalName, externalCount.get(i)));
            }
        }
    }

    private static ArrayList<String> getDownloadStorage(ArrayList<String> internalCount, ArrayList<String> externalCount) {
        ArrayList<String> downloadCount = new ArrayList<>();
        final String folderName = "download";

        File[] download = new File(AllStorageAware.ROOT_PATH).listFiles();
        for (File value : download) {
            if (value.isDirectory() && value.getName().equalsIgnoreCase(folderName))
                downloadCount.add(value.getPath());
        }

        if (downloadCount.size() == 0 && internalCount.size() != 0) {
            download = new File(internalCount.get(0)).listFiles();
            for (File value : download) {
                if (value.isDirectory() && value.getName().equalsIgnoreCase(folderName))
                    downloadCount.add(value.getPath());
            }
        }

        if (downloadCount.size() == 0 && externalCount.size() != 0) {
            download = new File(externalCount.get(0)).listFiles();
            for (File value : download) {
                if (value.isDirectory() && value.getName().equalsIgnoreCase(folderName))
                    downloadCount.add(value.getPath());
            }
        }
        return downloadCount;
    }
}
