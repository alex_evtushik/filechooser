package ru.bossnote.filechooser.ui.fragments;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import ru.bossnote.filechooser.R;
import ru.bossnote.filechooser.adapters.SimpleSpinnerAdapter;
import ru.bossnote.filechooser.adapters.VideoDataAdapter;
import ru.bossnote.filechooser.constants.Constants;
import ru.bossnote.filechooser.cursors.VideoLoaderCallback;
import ru.bossnote.filechooser.listeners.OnAlbumClickListener;
import ru.bossnote.filechooser.listeners.OnFinishCallback;
import ru.bossnote.filechooser.listeners.PauseOnScrollListener;
import ru.bossnote.filechooser.models.GalleryInfo;
import ru.bossnote.filechooser.models.ReturnableFileInfo;

/**
 * Created by Alexandr on 02.03.2015.
 */
public class VideoFragment extends BaseFragment {
    public static final String TAG = VideoFragment.class.getSimpleName();
    private final int SPAN_COUNT = 2;
    private RecyclerView mRecyclerView;
    private GridLayoutManager mLayoutManager;
    private VideoDataAdapter mAdapter;
    private VideoLoaderCallback mLoader;
    private LoaderManager mLoaderManager;
    private Toolbar mToolbar;
    private View mSpinnerContainer;
    private Spinner mSpinner;
    private SimpleSpinnerAdapter mSpinnerAdapter;
    private ArrayList<GalleryInfo> mAlbumDepth;

    public VideoFragment() {
    }

    public static VideoFragment newInstance() {
        VideoFragment fragment = new VideoFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.recycler_view_fragment, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
        mAdapter = new VideoDataAdapter(null);
        mLoaderManager = getLoaderManager();
        mLoader = new VideoLoaderCallback(getActivity(), mAdapter);
        mAdapter.setOnAlbumClickListener(onAlbumClick);
        mAdapter.setOnFinishCallback(callback);
        mRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mRecyclerView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        int viewWidth = mRecyclerView.getMeasuredWidth();
                        float cardViewWidth = getActivity().getResources().getDimension(R.dimen.cardview_layout_width);
                        int newSpanCount = (int) Math.floor(viewWidth / cardViewWidth);
                        mLayoutManager.setSpanCount(newSpanCount);
                        mLayoutManager.requestLayout();
                    }
                });

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance()));
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mSpinnerContainer = LayoutInflater.from(getActivity()).inflate(R.layout.toolbar_spinner, mToolbar, false);
        mToolbar.addView(mSpinnerContainer);
        mSpinner = (Spinner) mSpinnerContainer.findViewById(R.id.toolbar_spinner);
        mSpinnerAdapter = new SimpleSpinnerAdapter(getActivity());
        mSpinner.setAdapter(mSpinnerAdapter);
        mSpinner.setOnItemSelectedListener(onItemSelected);

        if (savedInstanceState != null)
            mAlbumDepth = savedInstanceState.getParcelableArrayList(TAG);
        else {
            mAlbumDepth = new ArrayList<>();
            mAlbumDepth.add(new GalleryInfo(getActivity().getResources().getString(R.string.movies), "", true));
        }

        updateCurrentAlbum();

        return rootView;
    }

    private void updateCurrentAlbum() {
        mSpinnerAdapter.swapData(mAlbumDepth);
        mSpinner.setSelection(mAlbumDepth.size() - 1);
        boolean isRoot = mAlbumDepth.get(mAlbumDepth.size() - 1).isRoot();

        if (isRoot) {
            mLoaderManager.destroyLoader(VideoLoaderCallback.VIDEO_LOADER);
            mLoaderManager.initLoader(VideoLoaderCallback.ALBUM_LOADER, null, mLoader);
        } else {
            mLoaderManager.destroyLoader(VideoLoaderCallback.ALBUM_LOADER);
            String albumId = mAlbumDepth.get(mAlbumDepth.size() - 1).getAlbumId();
            mLoader.setPictureWhere(albumId);
            mLoaderManager.initLoader(VideoLoaderCallback.VIDEO_LOADER, null, mLoader);
        }

        if (mAlbumDepth.size() <= 1) {
            mSpinner.setClickable(false);
        } else {
            mSpinner.setClickable(true);
        }

    }

    @Override
    public boolean onBackPressed() {
        if (mAdapter.getActionMode() != null) {
            mAdapter.getActionMode().finish();
            return true;
        } else if (mAlbumDepth.size() > 1) {
            mAlbumDepth.remove(mAlbumDepth.size() - 1);
            updateCurrentAlbum();
            return true;
        } else {
            getActivity().setResult(Activity.RESULT_CANCELED);
            getActivity().finish();
            return false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mToolbar.removeView(mSpinnerContainer);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(TAG, new ArrayList<>(mAlbumDepth));
    }

    private AdapterView.OnItemSelectedListener onItemSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            for (int i = mAlbumDepth.size() - 1; i > position; i--) {
                mAlbumDepth.remove(i);
            }
            updateCurrentAlbum();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private OnAlbumClickListener onAlbumClick = new OnAlbumClickListener() {
        @Override
        public void onAlbumClick(String albumName, String albumId) {
            mAlbumDepth.add(new GalleryInfo(albumName, albumId, false));
            updateCurrentAlbum();
        }
    };

    private OnFinishCallback callback = new OnFinishCallback() {
        @Override
        public void onFinish(List<ReturnableFileInfo> array) {
            if (array.size() == 0) {
                getActivity().setResult(Activity.RESULT_CANCELED);
                getActivity().finish();
            } else {
                Bundle args = new Bundle();
                args.putParcelableArrayList(Constants.RESULT_KEY, new ArrayList<>(array));
                Intent data = new Intent();
                data.putExtras(args);
                getActivity().setResult(Activity.RESULT_OK, data);
                getActivity().finish();
            }
        }
    };
}
