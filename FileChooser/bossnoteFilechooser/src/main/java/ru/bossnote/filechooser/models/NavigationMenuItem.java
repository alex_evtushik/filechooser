package ru.bossnote.filechooser.models;

/**
 * Created by Alexandr on 26.02.2015.
 */
public class NavigationMenuItem {
    private int mImage;
    private String mText;
    private String mPath;

    public NavigationMenuItem(int image, String text) {
        this(image, text, "");
    }

    public NavigationMenuItem(int image, String text, String path) {
        mImage = image;
        mText = text;
        mPath = path;
    }

    public int getImage() {
        return mImage;
    }

    public String getText() {
        return mText;
    }

    public String getPath() {
        return mPath;
    }
}
