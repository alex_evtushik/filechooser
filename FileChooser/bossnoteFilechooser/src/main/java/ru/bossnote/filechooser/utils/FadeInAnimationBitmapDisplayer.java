package ru.bossnote.filechooser.utils;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;

import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;

/**
 * Created by Alexandr on 13.03.2015.
 */
public class FadeInAnimationBitmapDisplayer implements BitmapDisplayer {
    private static final int FADE_IN_TIME = 200;
    private Resources mResources;
    public FadeInAnimationBitmapDisplayer(Resources resources) {
        mResources = resources;
    }

    @Override
    public void display(Bitmap bitmap, ImageAware imageAware, LoadedFrom loadedFrom) {
        if (LoadedFrom.MEMORY_CACHE == loadedFrom) {
            imageAware.setImageBitmap(bitmap);
        } else {
            final TransitionDrawable td = new TransitionDrawable(new Drawable[]{
                    new ColorDrawable(android.R.color.transparent),
                    new BitmapDrawable(mResources, bitmap)
            });
            imageAware.setImageDrawable(td);
            td.startTransition(FADE_IN_TIME);
        }
    }
}
