package ru.bossnote.filechooser.utils;

import android.os.Environment;
import android.os.StatFs;

import java.io.File;
import java.text.DecimalFormat;

/**
 * Created by Alexandr on 03.03.2015.
 */
public class SizeUtils {

    private SizeUtils() {}

    public static String getFileSize(long size) {
        if (size <= 0) return "0 B";
        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.##").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public static String getFileSize(String size) {
        return getFileSize(Long.parseLong(size));
    }

    public static int getDefaultLruMemoryCacheSize() {
        long maxMemory = Runtime.getRuntime().maxMemory(); // TODO ? bytes -> kb
        int cacheSize = (int) (maxMemory / 8);
        return cacheSize;
    }

    public static int getDefaultLruDiskSize() {
        File path = Environment.getExternalStorageDirectory();
        StatFs stat = new StatFs(path.getPath());
        int availBlocks = stat.getAvailableBlocks();
        int blockSize = stat.getBlockSize();
        long storageSize = (long) availBlocks * (long) blockSize;
        long free = storageSize / 8;
        return (int) free;
    }
}
