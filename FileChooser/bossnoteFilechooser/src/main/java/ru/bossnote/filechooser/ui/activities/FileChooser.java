package ru.bossnote.filechooser.ui.activities;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

import com.nostra13.universalimageloader.cache.memory.impl.LRULimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.util.ArrayList;

import ru.bossnote.filechooser.R;
import ru.bossnote.filechooser.constants.Constants;
import ru.bossnote.filechooser.listeners.DialogClickListener;
import ru.bossnote.filechooser.models.NavigationMenuItem;
import ru.bossnote.filechooser.ui.fragments.AlertDialogFragment;
import ru.bossnote.filechooser.ui.fragments.AudioFragment;
import ru.bossnote.filechooser.ui.fragments.BaseFragment;
import ru.bossnote.filechooser.ui.fragments.FilesDirectoryFragment;
import ru.bossnote.filechooser.ui.fragments.NavigationDrawerFragment;
import ru.bossnote.filechooser.ui.fragments.PictureFragment;
import ru.bossnote.filechooser.ui.fragments.VideoFragment;
import ru.bossnote.filechooser.utils.FadeInAnimationBitmapDisplayer;
import ru.bossnote.filechooser.utils.NavigationMenuUtils;
import ru.bossnote.filechooser.utils.SizeUtils;
import ru.bossnote.filechooser.utils.ThumbnailDownloader;


public class FileChooser extends ActionBarActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks {
    private static final String TAG = FileChooser.class.getSimpleName();
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar mToolbar;
    private boolean isFull = true;
    private ArrayList<NavigationMenuItem> items = new ArrayList<>();
    public static DisplayImageOptions sBaseDisplayOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ativity_filechooser);

        isFull = getIntent().getBooleanExtra(Constants.FULL_VERSION_KEY, false);
        isFull = getIntent().getBooleanExtra(Constants.FULL_VERSION_KEY, false);

        items = NavigationMenuUtils.getListMenu(getResources());

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        initImageLoader();

        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar, items);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BaseFragment fragment = (BaseFragment) getFragmentManager().findFragmentById(R.id.container);
        if (fragment == null) {
            mNavigationDrawerFragment.selectItem(Constants.PICTURE);
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        if (isFull)
            fullVersion(position);
        else
            basicVersion(position);

    }

    private void basicVersion(int position) {
        switch (position) {
            case Constants.PICTURE:
                showFragmentWithHandler(PictureFragment.newInstance());
                break;
            default:
                showPurchaseDialog();
        }
    }

    private void showPurchaseDialog() {
        final AlertDialogFragment dialogFragment = AlertDialogFragment.newInstance(getResources().getString(R.string.dialog_message_buy_goods));
        dialogFragment.setCallback(new DialogClickListener() {
            @Override
            public void onPositiveClick() {
                dialogFragment.dismiss();
            }

            @Override
            public void onNegativeClick() {
                dialogFragment.dismiss();
            }

        });
        dialogFragment.show(getFragmentManager(), AlertDialogFragment.TAG);
    }

    private void fullVersion(int position) {
        switch (position) {
            case Constants.PICTURE:
                showFragment(PictureFragment.newInstance());
                break;
            case Constants.VIDEO:
                showFragment(VideoFragment.newInstance());
                break;
            case Constants.AUDIO:
                showFragmentWithHandler(AudioFragment.newInstance());
                break;
            default:
                showFragmentWithHandler(FilesDirectoryFragment.newInstance(items.get(position).getText(), items.get(position).getPath()));
        }
    }

    private void showFragment(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }


    private void showFragmentWithHandler(final Fragment fragment) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showFragment(fragment);
            }
        }, Constants.DELAY_MILLIS);
    }

    private void initImageLoader() {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inPreferredConfig = Bitmap.Config.RGB_565;
        sBaseDisplayOptions = new DisplayImageOptions.Builder()
                .displayer(new FadeInAnimationBitmapDisplayer(getResources()))
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .decodingOptions(opts)
                .delayBeforeLoading(100)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
//                .writeDebugLogs() // TODO Enabled logs
                .defaultDisplayImageOptions(sBaseDisplayOptions)
                .imageDownloader(new ThumbnailDownloader(this))
                .memoryCache(new LRULimitedMemoryCache(SizeUtils.getDefaultLruMemoryCacheSize()))
                .memoryCacheSize(SizeUtils.getDefaultLruMemoryCacheSize())
//                .diskCacheSize(SizeUtils.getDefaultLruDiskSize())
                        //.diskCacheExtraOptions(320, 320, null)
                .build();

        ImageLoader loader = ImageLoader.getInstance();
        if (loader.isInited()) {
            loader.stop();
            loader.destroy();
        }
        loader.init(config);
    }

    @Override
    public void onBackPressed() {
        BaseFragment fragment = (BaseFragment) getFragmentManager().findFragmentById(R.id.container);
        if (fragment != null && !fragment.onBackPressed())
            super.onBackPressed();
    }
}