package ru.bossnote.filechooser.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfRenderer;
import android.media.ThumbnailUtils;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.Log;

import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.qoppa.android.pdf.PDFException;
import com.qoppa.android.pdfProcess.PDFDocument;
import com.qoppa.android.pdfViewer.fonts.StandardFontTF;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Created by Alexandr on 06.03.2015.
 */
public class ThumbnailDownloader extends BaseImageDownloader {
    private static final String TAG = ThumbnailDownloader.class.getSimpleName();
    public static final String URI_VIDEO_PREFIX = "video://";
    public static final String URI_PDF_PREFIX = "pdf://";
    private final ReentrantLock mLock = new ReentrantLock();

    public ThumbnailDownloader(Context context) {
        super(context);

        //this static allows the sdk to access font assets,
        //it must be set prior to utilizing libraries
        StandardFontTF.mAssetMgr = context.getAssets();
    }

    @Override
    public InputStream getStream(String uri, Object extra) throws IOException {
        if (videoBelongsTo(uri))
            return getVideoThumbnailStream(uri);
        else if (pdfBelongsTo(uri))
            return getPDFThumbnailStream(uri);
        else
            return super.getStream(uri, extra);
    }

    private boolean videoBelongsTo(String uri) {
        return uri.toLowerCase(Locale.ENGLISH).startsWith(URI_VIDEO_PREFIX);
    }

    private boolean pdfBelongsTo(String uri) {
        return uri.toLowerCase(Locale.ENGLISH).startsWith(URI_PDF_PREFIX);
    }

    private InputStream getVideoThumbnailStream(String uri) {
        String imageUri = uri.substring(URI_VIDEO_PREFIX.length());
        Log.d(TAG, "imageUri = " + imageUri);
        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(imageUri, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
        if (bitmap != null) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
            return new ByteArrayInputStream(bos.toByteArray());
        } else
            return null;
    }

    private InputStream getPDFThumbnailStream(String uri) {
        String pdfUri = uri.substring(URI_PDF_PREFIX.length());
        Log.d(TAG, "imageUri = " + pdfUri);

        File file = new File(pdfUri);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mLock.lock();
            try {
                Log.d(TAG, "Lollipop = " + file.getName());
                ParcelFileDescriptor descriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
                PdfRenderer renderer = new PdfRenderer(descriptor);

                PdfRenderer.Page page = renderer.openPage(0);

                Bitmap bitmap = Bitmap.createBitmap(page.getWidth(), page.getHeight(), Bitmap.Config.ARGB_8888);

                page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);

                page.close();
                renderer.close();
                return new ByteArrayInputStream(outputStream.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                mLock.unlock();
            }

            return null;
        } else {
            try {

                //this static allows the sdk to access font assets,
                //it must be set prior to utilizing libraries
                StandardFontTF.mAssetMgr = context.getAssets();

                // Load a document and get the first page
                PDFDocument pdf = new PDFDocument(pdfUri, null);
                com.qoppa.android.pdfProcess.PDFPage page = pdf.getPage(0);

                // Create a bitmap and canvas to draw the page into
                int width = (int) Math.ceil(page.getDisplayWidth());
                int height = (int) Math.ceil(page.getDisplayHeight());
                Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

                // Create canvas to draw into the bitmap
                Canvas c = new Canvas(bitmap);

                // Fill the bitmap with a white background
                Paint whiteBgnd = new Paint();
                whiteBgnd.setColor(Color.WHITE);
                whiteBgnd.setStyle(Paint.Style.FILL);
                c.drawRect(0, 0, width, height, whiteBgnd);

                // paint the page into the canvas
                page.paintPage(c);


                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);

                return new ByteArrayInputStream(outputStream.toByteArray());

            } catch (PDFException e) {
                e.printStackTrace();
            }
            return null;

        }
    }
}
