package ru.bossnote.filechooser.utils;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Alexandr on 05.03.2015.
 */
public class FileTypeUtils {

    public static enum FILE_TYPE {
        IMAGE, VIDEO, AUDIO,
        GEO,
        DOC, ARCHIVE, SPDATA,
        UNSUPPORTED;
    }

    private static final String[] IMAGE_FORMATS = {"jpeg", "jpg", "png", "gif", "tiff", "ps"};
    private static final String[] VIDEO_FORMATS = {"3gp", "mp4", "avi", "flv", "wmv", "mkv", "mov", "vob"};
    private static final String[] AUDIO_FORMATS = {"aac", "mp3", "wav", "amr", "m4a", "m4b", "m4p", "m4r",
            "mid", "ape", "wma", "ogg", "flac", "mpeg"};
    private static final String[] DOC_FORMATS = {"doc", "docx", "docm", "xls", "xlsx", "xlsm", "txt",
            "odt", "sxw", "fb2", "pdf", "djvu"};
    private static final String[] ARCHIVE_FORMATS = {"7z", "rar", "zip"};
    private static final String[] GEO_FORMATS = {"geo", "json"};

    // TODO Можно оптимизировать сделав HashMap и загнав все в одну хэш мапу где значение было бы типом enum константы
    private static Map<FILE_TYPE, HashSet<String>> extensionsSource = new Hashtable<FILE_TYPE, HashSet<String>>();

    static {
        extensionsSource.put(FILE_TYPE.IMAGE, new HashSet<String>(Arrays.asList(IMAGE_FORMATS)));
        extensionsSource.put(FILE_TYPE.VIDEO, new HashSet<String>(Arrays.asList(VIDEO_FORMATS)));
        extensionsSource.put(FILE_TYPE.AUDIO, new HashSet<String>(Arrays.asList(AUDIO_FORMATS)));
        extensionsSource.put(FILE_TYPE.DOC, new HashSet<String>(Arrays.asList(DOC_FORMATS)));
        extensionsSource.put(FILE_TYPE.ARCHIVE, new HashSet<String>(Arrays.asList(ARCHIVE_FORMATS)));
        extensionsSource.put(FILE_TYPE.GEO, new HashSet<String>(Arrays.asList(GEO_FORMATS)));
        extensionsSource.put(FILE_TYPE.SPDATA, new HashSet<String>(Arrays.asList(new String[]{"spdata"})));
    }

    public static FILE_TYPE getFileType(File f) {
        return getFileType(getExtension(f));
    }

    private static String getExtension(File f) {
        String filename = f.getName();
        return getExtension(filename);
    }

    private static String getExtension(String path) {
        return path.substring(path.lastIndexOf('.') + 1, path.length());
    }

    private synchronized static FILE_TYPE getFileType(String ext) {
        FILE_TYPE fileType = FILE_TYPE.UNSUPPORTED;
        HashSet<String> hash;
        for (Map.Entry<FILE_TYPE, HashSet<String>> type : extensionsSource.entrySet()) {
            hash = type.getValue();
            if (hash.contains(ext.toLowerCase(Locale.ENGLISH))) {
                fileType = type.getKey();
            }
        }
        return fileType;
    }

    public static String getFileMimeType(String url) {
        final String UNDEFINED_TYPE = "*/*";
        String type = UNDEFINED_TYPE;
        int index = url.lastIndexOf('.');

        if (index < 0 || index > url.length() - 1) {
            return UNDEFINED_TYPE;
        }
        String newUrl = url.substring(index).toLowerCase();
        Log.d("TAG", " url = " + newUrl);
        String extension = MimeTypeMap.getFileExtensionFromUrl(newUrl);
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);

            if (type == null)
                return UNDEFINED_TYPE;
        }
        return type;
    }
}
