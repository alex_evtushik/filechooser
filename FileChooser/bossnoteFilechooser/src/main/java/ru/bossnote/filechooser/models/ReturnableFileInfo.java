package ru.bossnote.filechooser.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alexandr on 16.03.2015.
 */
public class ReturnableFileInfo implements Parcelable {
    private String mUri;
    private String mName;
    private long mSize;
    private String mType;

    public ReturnableFileInfo(String uri, String name, long size, String type) {
        mUri = uri;
        mName = name;
        mSize = size;
        mType = type;
    }

    public ReturnableFileInfo(Parcel parcel) {
        String[] array = new String[4];
        parcel.readStringArray(array);
        mUri = array[0];
        mName = array[1];
        mSize = Long.valueOf(array[2]);
        mType = array[3];



    }

    public String getUri() {
        return mUri;
    }

    public String getName() {
        return mName;
    }

    public long getSize() {
        return mSize;
    }

    public String getType() {
        return mType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] { mUri, mName, String.valueOf(mSize), mType});
    }

    public static final Parcelable.Creator<ReturnableFileInfo> CREATOR = new Parcelable.Creator<ReturnableFileInfo>() {
        @Override
        public ReturnableFileInfo createFromParcel(Parcel source) {
            return new ReturnableFileInfo(source);
        }

        @Override
        public ReturnableFileInfo[] newArray(int size) {
            return new ReturnableFileInfo[0];
        }
    };
}
