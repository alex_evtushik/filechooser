package ru.bossnote.filechooser.listeners;

/**
 * Created by Alexandr on 14.03.2015.
 */
public interface DialogClickListener {

    public void onPositiveClick();
    public void onNegativeClick();
}
