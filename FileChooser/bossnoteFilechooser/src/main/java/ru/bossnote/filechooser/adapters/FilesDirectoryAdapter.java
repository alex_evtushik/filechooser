package ru.bossnote.filechooser.adapters;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ru.bossnote.filechooser.R;
import ru.bossnote.filechooser.handlers.ContentHandler;
import ru.bossnote.filechooser.handlers.ProcessingFilesTask;
import ru.bossnote.filechooser.listeners.OnFinishCallback;
import ru.bossnote.filechooser.listeners.OnFolderClickLister;
import ru.bossnote.filechooser.models.FileInfo;
import ru.bossnote.filechooser.models.ReturnableFileInfo;
import ru.bossnote.filechooser.ui.activities.FileChooser;
import ru.bossnote.filechooser.utils.FileTypeUtils;
import ru.bossnote.filechooser.utils.SizeUtils;
import ru.bossnote.filechooser.utils.ThumbnailDownloader;

/**
 * Created by Alexandr on 03.03.2015.
 */
public class FilesDirectoryAdapter extends SelectableAdapter<FilesDirectoryAdapter.ViewHolder> {
    public static final String TAG = FilesDirectoryAdapter.class.getSimpleName();
    private List<FileInfo> mItems;
    private Context mContext;
    private ActionMode mActionMode;
    private OnFolderClickLister onFolderClickLister;
    private OnFinishCallback onFinishCallback;
    private ImageLoader mImageLoader;
    private DisplayImageOptions mOptions;
    private FrameLayout mProgressBar;

    public FilesDirectoryAdapter() {
        mImageLoader = ImageLoader.getInstance();
    }

    @Override
    public FilesDirectoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mProgressBar = (FrameLayout) ((ActionBarActivity) mContext).findViewById(R.id.progress_bar);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.content_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FilesDirectoryAdapter.ViewHolder holder, int position) {
        if (mItems != null && holder != null) {
            holder.fill(mItems.get(position), isSelectes(position));
        }
    }

    /***/
    public void swapData(List<FileInfo> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setOnFolderClickLister(OnFolderClickLister lister) {
        onFolderClickLister = lister;
    }

    public void setOnFinishCallback(OnFinishCallback callback) {
        onFinishCallback = callback;
    }

    public void selection(int position) {
        toggleSelection(position);
        int count = getSelectedItemCount();
        if (count == 0)
            mActionMode.finish();
        else {
            mActionMode.setTitle(String.valueOf(count));
            mActionMode.invalidate();
        }
    }

    private ActionMode.Callback callback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            actionMode.getMenuInflater().inflate(R.menu.selected_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            if (menuItem.getItemId() == R.id.select) {
                List<Integer> selectedPos = getSelectedItems();
                Log.d(TAG, "SELECTED count = " + selectedPos.size());

                ProcessingFilesTask task = new ProcessingFilesTask(mItems, selectedPos, mProgressBar, onFinishCallback);
                task.execute();
                actionMode.finish();
                return true;
            } else
                return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            mActionMode = null;
            clearSelection();
        }
    };

    public void calculateSelectedFiles(int index, List<FileInfo> items, List<String> outSelectedFiles) {
        if (items.get(index).isDirectiry()) {
            String path = items.get(index).getPath();
            ArrayList<FileInfo> innerItems = ContentHandler.getFiles(path);
            for (int i = 0; i < innerItems.size(); i++) {
                calculateSelectedFiles(i, innerItems, outSelectedFiles);
            }
        } else {
            outSelectedFiles.add(items.get(index).getFileUri());
        }
    }

    public ActionMode getActionMode() {
        return mActionMode;
    }

    /***/
    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageViewIcon;
        public TextView mTextViewName;
        public TextView mTextViewDate;
        public TextView mTextViewSize;
        public ImageView mImageViewSelected;

        public ViewHolder(View itemView) {
            super(itemView);

            mImageViewIcon = (ImageView) itemView.findViewById(R.id.item_icon);
            mTextViewName = (TextView) itemView.findViewById(R.id.item_text_name);
            mTextViewDate = (TextView) itemView.findViewById(R.id.item_text_date);
            mTextViewSize = (TextView) itemView.findViewById(R.id.item_text_size);
            mImageViewSelected = (ImageView) itemView.findViewById(R.id.selected_icon);

            itemView.setOnClickListener(clickListener);
            itemView.setOnLongClickListener(longClickListener);
        }

        public void fill(FileInfo info, boolean isSelected) {
            String data = info.getData();
            mImageViewIcon.setImageResource(info.getImage());


            if (!info.isDirectiry()) {
                switch (FileTypeUtils.getFileType(new File(info.getPath()))) {
                    case IMAGE:
                        mOptions = new DisplayImageOptions.Builder().cloneFrom(FileChooser.sBaseDisplayOptions)
                                .showImageOnLoading(R.drawable.ic_picture_list_item).build();
                        mImageLoader.displayImage("file://" + info.getPath(), mImageViewIcon, mOptions);
                        break;
                    case VIDEO:
                        mOptions = new DisplayImageOptions.Builder().cloneFrom(FileChooser.sBaseDisplayOptions)
                                .showImageOnLoading(R.drawable.ic_video_list_item).build();
                        mImageLoader.displayImage(ThumbnailDownloader.URI_VIDEO_PREFIX + info.getPath(), mImageViewIcon, mOptions);
                        break;
                }

                if (info.getPath().contains(".pdf")) {
                    mOptions = new DisplayImageOptions.Builder().cloneFrom(FileChooser.sBaseDisplayOptions)
                            .showImageOnLoading(R.drawable.ic_file_list_item).build();
                    mImageLoader.displayImage(ThumbnailDownloader.URI_PDF_PREFIX + info.getPath(), mImageViewIcon, mOptions);
                }

                data = SizeUtils.getFileSize(info.getData());
            }

            mTextViewName.setText(info.getName());
            mTextViewDate.setText(info.getDate());
            mTextViewSize.setText(data);
            mImageViewSelected.setVisibility(isSelected ? View.VISIBLE : View.INVISIBLE);
        }

        private View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mActionMode != null)
                    selection(getPosition());
                else if (mItems.get(getPosition()).isDirectiry()) {
                    onFolderClickLister.onFolderClick(mItems.get(getPosition()));
                } else {
                    List<ReturnableFileInfo> selectedFilesUri = new ArrayList<>();

                    String uri = mItems.get(getPosition()).getFileUri();
                    String name = mItems.get(getPosition()).getName();
                    long size = Long.parseLong(mItems.get(getPosition()).getData());
                    String mimeType = FileTypeUtils.getFileMimeType(name);

                    selectedFilesUri.add(new ReturnableFileInfo(uri, name, size, mimeType));
                    onFinishCallback.onFinish(selectedFilesUri);
                }
            }
        };

        private View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mActionMode == null) {
                    mActionMode = ((ActionBarActivity) mContext).startSupportActionMode(callback);
                    selection(getPosition());
                }
                return true;
            }
        };
    }
}
