package ru.bossnote.filechooser.cursors;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;

import ru.bossnote.filechooser.adapters.PictureDataAdapter;

/**
 * Created by Alexandr on 26.02.2015.
 */
public class PictureLoaderCallback implements LoaderManager.LoaderCallbacks<Cursor> {
    public static final int ALBUM_LOADER = 1;
    public static final int PICTURE_LOADER = 2;
    private final Uri CONTENT_URI = Media.EXTERNAL_CONTENT_URI;
    private final String[] PROJECTION_PICTURE = { Media.DATA, Media.DISPLAY_NAME, Media.SIZE, Media.MIME_TYPE};
    private final String[] PROJECTION_ALBUM = { Media.BUCKET_ID, Media.BUCKET_DISPLAY_NAME, Media.DATA};
    private final String GROUP_BY = Media.BUCKET_ID + ") GROUP BY ("+ Media.BUCKET_ID ;
    private final String SORT_ORDER = Media.DATE_ADDED + " DESC";
    private String mWhere = null;
    private CursorLoader mLoader = null;
    private Context mContext;
    private PictureDataAdapter mAdapter;

    public PictureLoaderCallback(Context context, PictureDataAdapter adapter) {
        mContext = context;
        mAdapter = adapter;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case ALBUM_LOADER:
                mLoader = new CursorLoader(mContext, CONTENT_URI, PROJECTION_ALBUM, GROUP_BY, null, SORT_ORDER);
                break;
            case PICTURE_LOADER:
                mLoader = new CursorLoader(mContext, CONTENT_URI, PROJECTION_PICTURE, mWhere, null, SORT_ORDER);
                break;
        }
        return mLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case ALBUM_LOADER:
                mAdapter.setOpenedAlbum(false);
                break;
            case PICTURE_LOADER:
                mAdapter.setOpenedAlbum(true);
                break;
            default:
                break;
        }
        mAdapter.swapCursor(data);
        mWhere = null;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    public void setPictureWhere(String albumId) {
        mWhere = Media.BUCKET_ID + "=" + albumId;
    }
}
