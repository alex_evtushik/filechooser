package ru.bossnote.filechooser.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alexandr on 10.03.2015.
 */
public class GalleryInfo extends SpinnerItem implements Parcelable {
    private String mAlbumId;
    private boolean mRoot;

    public GalleryInfo(String name, String albumId, boolean root) {
        super(name);
        mAlbumId = albumId;
        mRoot = root;
    }

    public GalleryInfo(Parcel parcel) {
        super();
        String[] array = new String[3];
        parcel.readStringArray(array);
        mName = array[0];
        mAlbumId = array[1];
        mRoot = Boolean.valueOf(array[2]);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    public String getAlbumId() {
        return mAlbumId;
    }

    public boolean isRoot() {
        return mRoot;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] { mName, mAlbumId, String.valueOf(mRoot)});
    }

    public static final Creator<GalleryInfo> CREATOR = new Creator<GalleryInfo>() {
        @Override
        public GalleryInfo createFromParcel(Parcel source) {
            return new GalleryInfo(source);
        }

        @Override
        public GalleryInfo[] newArray(int size) {
            return new GalleryInfo[0];
        }
    };
}
