package ru.bossnote.filechooser.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import ru.bossnote.filechooser.R;
import ru.bossnote.filechooser.adapters.FilesDirectoryAdapter;
import ru.bossnote.filechooser.adapters.SimpleSpinnerAdapter;
import ru.bossnote.filechooser.constants.Constants;
import ru.bossnote.filechooser.handlers.ContentHandler;
import ru.bossnote.filechooser.listeners.DialogClickListener;
import ru.bossnote.filechooser.listeners.OnFinishCallback;
import ru.bossnote.filechooser.listeners.OnFolderClickLister;
import ru.bossnote.filechooser.listeners.PauseOnScrollListener;
import ru.bossnote.filechooser.models.FileInfo;
import ru.bossnote.filechooser.models.ReturnableFileInfo;

/**
 * Created by Alexandr on 02.03.2015.
 */
public class FilesDirectoryFragment extends BaseFragment {

    public static final String TAG = FilesDirectoryFragment.class.getSimpleName();
    private static final String EXTRA_CATEGORY = "extra_category";
    private static final String EXTRA_PATH = "extra_path";
    private final int MAX_COUNT_FILES = 100;
    private RecyclerView mRecyclerView;
    private Toolbar mToolbar;
    private View mSpinnerContainer;
    private Spinner mSpinner;
    private LinearLayoutManager mLayoutManager;
    private FilesDirectoryAdapter mAdapter;
    private SimpleSpinnerAdapter mSpinnerAdapter;
    private ArrayList<FileInfo> mDirectoryDepth;

    public static FilesDirectoryFragment newInstance(String title, String path) {
        FilesDirectoryFragment fragment = new FilesDirectoryFragment();
        Bundle arg = new Bundle();
        arg.putString(EXTRA_CATEGORY, title);
        arg.putString(EXTRA_PATH, path);
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.recycler_view_fragment, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance()));
        mAdapter = new FilesDirectoryAdapter();
        mAdapter.setOnFolderClickLister(onFolderClick);
        mAdapter.setOnFinishCallback(callback);
        mRecyclerView.setAdapter(mAdapter);

        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        mSpinnerContainer = LayoutInflater.from(getActivity()).inflate(R.layout.toolbar_spinner, mToolbar, false);
        mToolbar.addView(mSpinnerContainer);
        mSpinner = (Spinner) mSpinnerContainer.findViewById(R.id.toolbar_spinner);
        mSpinnerAdapter = new SimpleSpinnerAdapter(getActivity());
        mSpinner.setAdapter(mSpinnerAdapter);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                for (int i = mDirectoryDepth.size() - 1; i > position; i--) {
                    mDirectoryDepth.remove(i);
                }
                updateCurrentDir();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (savedInstanceState != null) {
            mDirectoryDepth = savedInstanceState.getParcelableArrayList(TAG);
        } else {
            String categoryTitle = getArguments().getString(EXTRA_CATEGORY);
            String path = getArguments().getString(EXTRA_PATH);
            mDirectoryDepth = new ArrayList<>();
            mDirectoryDepth.add(new FileInfo(categoryTitle, "", "", path, 0, true));
        }

        updateCurrentDir();

        return rootView;
    }

    private void updateCurrentDir() {
        //Log.d(TAG, "updateCurrentDir() dirs: " + mDirectoryDepth);
        mSpinnerAdapter.swapData(mDirectoryDepth);
        mSpinner.setSelection(mDirectoryDepth.size() - 1);
        List<FileInfo> files = ContentHandler.getFiles(mDirectoryDepth.get(mDirectoryDepth.size() - 1).getPath());
        mAdapter.swapData(files);

        if (mDirectoryDepth.size() <= 1) {
            mSpinner.setClickable(false);
        } else {
            mSpinner.setClickable(true);
        }
    }

    private OnFolderClickLister onFolderClick = new OnFolderClickLister() {
        @Override
        public void onFolderClick(FileInfo clickedDirectoryInfo) {
            mDirectoryDepth.add(clickedDirectoryInfo);
            updateCurrentDir();
        }
    };

    private OnFinishCallback callback = new OnFinishCallback() {
        @Override
        public void onFinish(List<ReturnableFileInfo> array) {
            if (array.size() > MAX_COUNT_FILES) {
                showAlertDialog(array);
            } else if (array.size() == 0) {
                getActivity().setResult(Activity.RESULT_CANCELED);
                getActivity().finish();
            } else {
                sendSelectedFiles(array);
            }
        }
    };

    private void showAlertDialog(final List<ReturnableFileInfo> array) {
        String message = "Вы выбрали " + array.size() + " файлов!";

        final AlertDialogFragment dialogFragment = AlertDialogFragment.newInstance(message);
        dialogFragment.setCallback(new DialogClickListener() {
            @Override
            public void onPositiveClick() {
                sendSelectedFiles(array);
            }

            @Override
            public void onNegativeClick() {
                dialogFragment.dismiss();
            }
        });
        dialogFragment.show(getFragmentManager(), AlertDialogFragment.TAG);
    }

    private void sendSelectedFiles(List<ReturnableFileInfo> array) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(Constants.RESULT_KEY, new ArrayList<>(array));
        Intent data = new Intent();
        data.putExtras(args);
        getActivity().setResult(Activity.RESULT_OK, data);
        getActivity().finish();
    }

    @Override
    public boolean onBackPressed() {
        if (mAdapter.getActionMode() != null) {
            mAdapter.getActionMode().finish();
            return true;
        } else if (mDirectoryDepth.size() > 1) {
            mDirectoryDepth.remove(mDirectoryDepth.size() - 1);
            updateCurrentDir();
            return true;
        } else {
            getActivity().setResult(Activity.RESULT_CANCELED);
            getActivity().finish();
            return false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mToolbar.removeView(mSpinnerContainer);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(TAG, new ArrayList<>(mDirectoryDepth));
    }
}