package ru.bossnote.filechooser.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alexandr on 02.03.2015.
 */
public class FileInfo extends SpinnerItem implements Comparable<FileInfo>, Parcelable {
    private String mData;
    private String mDate;
    private String mPath;
    private int mImage;
    private boolean isDirectiry;

    public FileInfo(String name, String data, String date, String path, int image, boolean isDir) {
        super(name);
        mData = data;
        mDate = date;
        mPath = path;
        mImage = image;
        isDirectiry = isDir;
    }

    public FileInfo(Parcel parcel) {
        super();
        String[] array = new String[6];
        parcel.readStringArray(array);
        mName = array[0];
        mData = array[1];
        mDate = array[2];
        mPath = array[3];
        mImage = Integer.valueOf(array[4]);
        isDirectiry = Boolean.valueOf(array[5]);
    }

    public String getName() {
        return mName;
    }

    public String getData() {
        return mData;
    }

    public String getDate() {
        return mDate;
    }

    public String getPath() {
        return mPath;
    }

    public int getImage() {
        return mImage;
    }

    public boolean isDirectiry() {
        return isDirectiry;
    }

    @Override
    public int compareTo(FileInfo another) {
        if(this.mName != null)
            return this.mName.toLowerCase().compareTo(another.getName().toLowerCase());
        else
            throw new IllegalArgumentException();
    }

    public String getFileUri() {
        return "file://" + mPath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] { mName, mData, mDate, mPath, String.valueOf(mImage), String.valueOf(isDirectiry) });
    }

    public static final Parcelable.Creator<FileInfo> CREATOR = new Parcelable.Creator<FileInfo>() {
        @Override
        public FileInfo createFromParcel(Parcel source) {
            return new FileInfo(source);
        }

        @Override
        public FileInfo[] newArray(int size) {
            return new FileInfo[0];
        }
    };

}
