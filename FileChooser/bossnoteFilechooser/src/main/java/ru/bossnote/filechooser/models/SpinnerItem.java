package ru.bossnote.filechooser.models;

/**
 * Created by Ruslan on 07.03.2015.
 */
public class SpinnerItem {

    protected String mName;

    public SpinnerItem() {}

    public SpinnerItem(String name) {
        this.mName = name;
    }

    public String getName() {
        return mName;
    }
}
