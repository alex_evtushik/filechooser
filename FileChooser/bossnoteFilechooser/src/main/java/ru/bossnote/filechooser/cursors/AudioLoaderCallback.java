package ru.bossnote.filechooser.cursors;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Audio.Media;

import ru.bossnote.filechooser.adapters.AudioDataAdapter;

/**
 * Created by Alexandr on 27.02.2015.
 */
public class AudioLoaderCallback implements LoaderManager.LoaderCallbacks<Cursor> {
    public static final int ALBUM_LOADER = 1;
    public static final int AUDIO_LOADER = 2;
    private final Uri CONTENT_URI = Media.EXTERNAL_CONTENT_URI;
    private final String[] PROJECTION_AUDIO = {Media.DATA, Media.DISPLAY_NAME, Media.ARTIST, Media.DURATION, Media.SIZE, Media.MIME_TYPE };
    private final String[] PROJECTION_ALBUM = {Media.ALBUM_ID, Media.ALBUM, Media.DATA};
    private final String GROUP_BY = Media.ALBUM_ID + ") GROUP BY (" + Media.ALBUM_ID;
    private final String SORT_ORDER_AUDIO = Media.TITLE_KEY + " ASC";
    private final String SORT_ORDER_ALBUM = Media.ALBUM_KEY + " ASC";
    private String mWhere = null;
    private CursorLoader mLoader = null;
    private Context mContext;
    private AudioDataAdapter mAdapter;

    public AudioLoaderCallback(Context context, AudioDataAdapter adapter) {
        mContext = context;
        mAdapter = adapter;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case ALBUM_LOADER:
                mLoader = new CursorLoader(mContext, CONTENT_URI, PROJECTION_ALBUM, GROUP_BY, null, SORT_ORDER_ALBUM);
                break;
            case AUDIO_LOADER:
                mLoader = new CursorLoader(mContext, CONTENT_URI, PROJECTION_AUDIO, mWhere, null, SORT_ORDER_AUDIO);
                break;
        }
        return mLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case ALBUM_LOADER:
                mAdapter.setOpenedAlbum(false);
                break;
            case AUDIO_LOADER:
                mAdapter.setOpenedAlbum(true);
                break;
            default:
                break;
        }
        mAdapter.swapCursor(data);
        mWhere = null;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    public void setPictureWhere(String albumId) {
        mWhere = Media.ALBUM_ID + "=" + albumId;
    }
}
