package ru.bossnote.filechooser.listeners;

/**
 * Created by Alexandr on 27.02.2015.
 */
public interface OnAlbumClickListener {

    public void onAlbumClick(String albumName, String albumId);
}
