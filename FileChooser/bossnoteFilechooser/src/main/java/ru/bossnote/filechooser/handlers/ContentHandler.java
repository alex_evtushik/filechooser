package ru.bossnote.filechooser.handlers;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import ru.bossnote.filechooser.R;
import ru.bossnote.filechooser.models.FileInfo;
import ru.bossnote.filechooser.utils.DateUtils;
import ru.bossnote.filechooser.utils.FileTypeUtils;
import ru.bossnote.filechooser.utils.SizeUtils;

/**
 * Created by Alexandr on 02.03.2015.
 */
public class ContentHandler {
    public static final String TAG = ContentHandler.class.getSimpleName();

    private ContentHandler() {}

    public static ArrayList<FileInfo> getFiles(String path) {
        File[] dirs = new File(path).listFiles();

        ArrayList<FileInfo> dir = new ArrayList<>();
        ArrayList<FileInfo> file = new ArrayList<>();

        if (dirs != null) {
            for (File value : dirs) {
                String lastModified = DateUtils.convertDataFrom(value.lastModified());

                if (value.isDirectory()) {
                    File[] fileBuffer = value.listFiles();
                    int count = 0;
                    String countItem = "";
                    if (fileBuffer != null) {
                        count = fileBuffer.length;
                        countItem = count + " items";
                    }
                    if (count == 0)
                        countItem = count + " item";

                    dir.add(new FileInfo(value.getName(), countItem, lastModified,
                            value.getAbsolutePath(), R.drawable.ic_folder_grey, value.isDirectory()));
                } else {
//                    String size = SizeUtils.getFileSize(value.length());
                    String size = value.length() + "";
                    int fileIcon;

                    switch (FileTypeUtils.getFileType(value)) {
                        case IMAGE:
                            fileIcon = R.drawable.ic_picture_list_item;
                            break;
                        case VIDEO:
                            fileIcon = R.drawable.ic_video_list_item;
                            break;
                        case AUDIO:
                            fileIcon = R.drawable.ic_audio_list_item;
                            break;
                        default:
                            fileIcon = R.drawable.ic_file_list_item;
                            break;
                    }

                    file.add(new FileInfo(value.getName(), size, lastModified,
                            value.getAbsolutePath(), fileIcon, value.isDirectory()));
                }
            }
            Collections.sort(dir);
            Collections.sort(file);
            dir.addAll(file);
        }
        return dir;
    }
}
