package ru.bossnote.filechooser.ui.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import ru.bossnote.filechooser.R;
import ru.bossnote.filechooser.listeners.DialogClickListener;

import static android.content.DialogInterface.*;

/**
 * Created by Alexandr on 14.03.2015.
 */
public class AlertDialogFragment extends DialogFragment {
    public static final String TAG = AlertDialogFragment.class.getSimpleName();
    private DialogClickListener mCallback;

    public AlertDialogFragment() {
    }

    public static AlertDialogFragment newInstance(String message) {
        AlertDialogFragment fragment = new AlertDialogFragment();
        Bundle args = new Bundle();
        args.putString(TAG, message);
        fragment.setArguments(args);
        return  fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String message = getArguments().getString(TAG);
        return new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.dialog_title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mCallback != null)
                            mCallback.onPositiveClick();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mCallback != null)
                            mCallback.onNegativeClick();
                    }
                })
                .create();
    }

    public void setCallback(DialogClickListener callback) {
        mCallback = callback;
    }
}
