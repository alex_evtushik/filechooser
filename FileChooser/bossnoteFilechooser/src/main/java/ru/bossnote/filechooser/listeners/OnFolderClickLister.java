package ru.bossnote.filechooser.listeners;

import ru.bossnote.filechooser.models.FileInfo;

/**
 * Created by Alexandr on 04.03.2015.
 */
public interface OnFolderClickLister {

    public void onFolderClick(FileInfo fileInfo);
}
