package ru.bossnote.filechooser.ui.fragments;

import android.app.Fragment;

/**
 * Created by Alexandr on 05.03.2015.
 */
public abstract class BaseFragment extends Fragment {

    public abstract boolean onBackPressed();
}
