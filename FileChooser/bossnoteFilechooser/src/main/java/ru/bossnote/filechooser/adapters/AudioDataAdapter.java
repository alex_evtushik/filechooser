package ru.bossnote.filechooser.adapters;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore.Audio.Media;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.bossnote.filechooser.R;
import ru.bossnote.filechooser.listeners.OnAlbumClickListener;
import ru.bossnote.filechooser.listeners.OnFinishCallback;
import ru.bossnote.filechooser.models.ReturnableFileInfo;
import ru.bossnote.filechooser.utils.DateUtils;

/**
 * Created by Alexandr on 27.02.2015.
 */
public class AudioDataAdapter extends SelectableAdapter<AudioDataAdapter.ViewHolder> {
    public static final String TAG = AudioDataAdapter.class.getSimpleName();
    private boolean isOpenedAlbum = false;
    private Context mContext;
    private Cursor mCursor;
    private ActionMode mActionMode;
    private OnAlbumClickListener mOnAlbumClickListener;
    private OnFinishCallback onFinishCallback;

    public AudioDataAdapter(Cursor cursor) {
        mCursor = cursor;
    }


    @Override
    public AudioDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.audio_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AudioDataAdapter.ViewHolder holder, int position) {
        if (mCursor != null) {
            mCursor.moveToPosition(position);
            String album = "";
            String audio = "";
            String artist = "";
            String convertedDuration = "";

            if (isOpenedAlbum) {
                audio = mCursor.getString(mCursor.getColumnIndex(Media.DISPLAY_NAME));
                artist = mCursor.getString(mCursor.getColumnIndex(Media.ARTIST));
                String duration = mCursor.getString(mCursor.getColumnIndex(Media.DURATION));
                if (duration != null)
                    convertedDuration = DateUtils.convertTimeFrom(Long.parseLong(duration));
            } else
                album = mCursor.getString(mCursor.getColumnIndex(Media.ALBUM));

            holder.mTextViewDuraction.setText(convertedDuration);
            holder.mTextViewAudio.setText(audio);
            holder.mTextViewArtisit.setText(artist);
            holder.mTextViewAlbum.setText(album);
            holder.mSelected.setVisibility(isSelectes(position) ? View.VISIBLE : View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mCursor == null ? 0 : mCursor.getCount();
    }

    public void setOpenedAlbum(boolean state) {
        isOpenedAlbum = state;
    }

    public boolean isOpenedAlbum() {
        return isOpenedAlbum;
    }

    public void setOnAlbumClickListener(OnAlbumClickListener listener) {
        mOnAlbumClickListener = listener;
    }

    public void setOnFinishCallback(OnFinishCallback callback) {
        onFinishCallback = callback;
    }

    public Cursor swapCursor(Cursor cursor) {
        if (mCursor == cursor) {
            return null;
        }
        Cursor oldCursor = mCursor;
        mCursor = cursor;
        if (cursor != null) {
            this.notifyDataSetChanged();
        }
        return oldCursor;
    }

    public void selection(int position) {
        toggleSelection(position);
        int count = getSelectedItemCount();

        if (count == 0)
            mActionMode.finish();
        else {
            mActionMode.setTitle(String.valueOf(count));
            mActionMode.invalidate();
        }
    }

    private ActionMode.Callback callback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            actionMode.getMenuInflater().inflate(R.menu.selected_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            if (menuItem.getItemId() == R.id.select) {
                ArrayList<ReturnableFileInfo> selectedFiles = new ArrayList<>();
                List<Integer> selectedPos = getSelectedItems();
                Log.d(TAG, "SELECTED count = " + selectedPos.size());
                for (int i = selectedPos.size() - 1; i >= 0; i--) {
                    mCursor.moveToPosition(selectedPos.get(i));

                    fillFinishArray(selectedFiles);
                }
                actionMode.finish();
                onFinishCallback.onFinish(selectedFiles);
                return true;
            } else
                return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            mActionMode = null;
            clearSelection();
        }
    };

    private void fillFinishArray(ArrayList<ReturnableFileInfo> selectedFiles) {
        String uri = "file://" + mCursor.getString(mCursor.getColumnIndex(Media.DATA));
        String name = mCursor.getString(mCursor.getColumnIndex(Media.DISPLAY_NAME));
        long size = Long.parseLong(mCursor.getString(mCursor.getColumnIndex(Media.SIZE)));
        String type = mCursor.getString(mCursor.getColumnIndex(Media.MIME_TYPE));

        selectedFiles.add(new ReturnableFileInfo(uri, name, size, type));

        Log.d(TAG, "name = " + name + ", size = " + size + ", type = " + type);
    }

    public ActionMode getActionMode() {
        return mActionMode;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextViewAlbum;
        public TextView mTextViewAudio;
        public TextView mTextViewArtisit;
        public TextView mTextViewDuraction;
        public ImageView mSelected;

        public ViewHolder(View itemView) {
            super(itemView);

            mTextViewAlbum = (TextView) itemView.findViewById(R.id.item_text_album);
            mTextViewAudio = (TextView) itemView.findViewById(R.id.item_text_audio);
            mTextViewArtisit = (TextView) itemView.findViewById(R.id.item_text_artist);
            mTextViewDuraction = (TextView) itemView.findViewById(R.id.item_text_duration);
            mSelected = (ImageView) itemView.findViewById(R.id.selected_icon);

            itemView.setOnClickListener(clickListener);
            itemView.setOnLongClickListener(longClickListener);
        }

        private View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mActionMode != null)
                    selection(getPosition());
                else if (!isOpenedAlbum) {
                    mCursor.moveToPosition(getPosition());
                    String albumName = mCursor.getString(mCursor.getColumnIndex(Media.ALBUM));
                    String albumId = mCursor.getString(mCursor.getColumnIndex(Media.ALBUM_ID));
                    mOnAlbumClickListener.onAlbumClick(albumName, albumId);
                } else {
                    ArrayList<ReturnableFileInfo> selectedFiles = new ArrayList<>();
                    mCursor.moveToPosition(getPosition());
                    fillFinishArray(selectedFiles);
                    onFinishCallback.onFinish(selectedFiles);
                }
            }
        };

        private View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mActionMode == null && isOpenedAlbum) {
                    mActionMode = ((ActionBarActivity) mContext).startSupportActionMode(callback);
                    selection(getPosition());
                }
                return true;
            }
        };
    }
}
